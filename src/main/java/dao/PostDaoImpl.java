package dao;


import pojo.Post;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.DBUtil.getConnection;

public class PostDaoImpl implements PostDao {
    @Override
    public Post queryPost(int account) {
        return null;
    }

    @Override
    public List<Post> starPost() {
        return null;
    }

    @Override
    public List<Post> orderPost() {
        List<Post> posts=new ArrayList<>();
        Connection conn=getConnection();
        String sql="select * from post order by pro_time desc ";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            ResultSet result=sta.executeQuery();
            while(result.next()){
                Post post=new Post();
                post.setPostId(result.getInt("post_id"));
                post.setPostAccount(result.getInt("post_account"));
                post.setPostTitle(result.getString("post_title"));
                post.setPostType(result.getInt("post_type"));
                post.setPostContent(result.getString("post_content"));
                post.setProTimer(result.getTimestamp("pro_time"));
                post.setStarNum(result.getInt("star_num"));
                posts.add(post);
            }
            result.close();
            sta.close();
        }catch(SQLException e){
            System.out.println("查询全部帖子（排好序）失败");
        }
        return posts;
    }
//    插入新的帖子
    @Override
    public int insertPost(Post post) {
        Connection conn=getConnection();
        String sql="insert into post(post_account,post_title,post_type,post_content,pro_time) values(?,?,?,?,?)";
        int result=0;
        int postId=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,post.getPostAccount());
            sta.setString(2,post.getPostTitle());
            sta.setInt(3,post.getPostType());
            sta.setString(4,post.getPostContent());
            sta.setTimestamp(5,post.getProTimer());
            result=sta.executeUpdate();
            if(result==1){
                sta=conn.prepareStatement("SELECT last_insert_id()");
                ResultSet resultSet=sta.executeQuery();
                while (resultSet.next()) {
                    postId=resultSet.getInt(1);
                }
            }
            sta.close();
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("插入新的帖子操作失败");
        }
        return postId;
    }
//    更新帖子信息
    @Override
    public boolean updatePost(Post post) {
        Connection conn=getConnection();
        String sql="update post set post_title=?,post_type=?,post_content=?,pro_time=?" +
                "where post_id=?";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setString(1,post.getPostTitle());
            sta.setInt(2,post.getPostType());
            sta.setString(3,post.getPostContent());
            sta.setTimestamp(4,post.getProTimer());
            sta.setInt(5,post.getPostId());
            result=sta.executeUpdate();
            sta.close();
        }catch(SQLException e){
            System.out.println("更新帖子操作失败");
        }
        return result==1?true:false;
    }
//    删除帖子
    @Override
    public boolean delPost(int id) {
        Connection conn=getConnection();
        String sql="delete from post where post_id=?";
        String sql1="delete from star where post_id=?";
        String sql2="delete from star where post_id=?";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,id);
            result=sta.executeUpdate();
            sta=conn.prepareStatement(sql1);
            sta.executeUpdate();
            sta=conn.prepareStatement(sql2);
            sta.executeUpdate();
            sta.close();
        }catch(SQLException e){
            System.out.println("删除帖子操作失败");
        }
        return result==1?true:false;
    }
}
