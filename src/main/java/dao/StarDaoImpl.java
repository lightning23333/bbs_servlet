package dao;

import pojo.Star;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.DBUtil.getConnection;

public class StarDaoImpl implements StarDao {
    @Override
    public List<Star> postStar(int postId) {
        Connection conn=getConnection();
        String sql="select * from star where post_id=?";
        List<Star> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,postId);
            ResultSet result=sta.executeQuery();
            while(result.next()){
                Star star=new Star();
                star.setStarId(result.getInt("star_id"));
                star.setPostId(postId);
                star.setStarAccount(result.getInt("star_account"));
                star.setStarTime(result.getTimestamp("star_time"));
                lists.add(star);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询指定帖子的点赞信息数据库操作失败");
        }
        return lists;
    }

    @Override
    public List<Star> queryAll() {
        Connection conn=getConnection();
        String sql="select * from star";
        List<Star> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            ResultSet result=sta.executeQuery();
            while(result.next()){
                Star star=new Star();
                star.setStarId(result.getInt("star_id"));
                star.setPostId(result.getInt("post_id"));
                star.setStarAccount(result.getInt("star_account"));
                star.setStarTime(result.getTimestamp("star_time"));
                lists.add(star);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询点赞全部信息数据库操作失败");
        }
        return lists;
    }

    @Override
    public List<Star> useStar(int account) {
        Connection conn=getConnection();
        String sql="select * from star where account=?";
        List<Star> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            ResultSet result=sta.executeQuery();
            while(result.next()){
                Star star=new Star();
                star.setStarId(result.getInt("star_id"));
                star.setPostId(result.getInt("post_id"));
                star.setStarAccount(account);
                star.setStarTime(result.getTimestamp("star_time"));
                lists.add(star);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询指定帖子的点赞信息数据库操作失败");
        }
        return lists;
    }

    @Override
    public int addStar(Star star){
        Connection conn=getConnection();
        String sql1="insert into star(post_id,star_account,star_time) values (?,?,?)";
        String sql2="SELECT LAST_INSERT_ID()";
        int result=-1;
        try{
            PreparedStatement sta=conn.prepareStatement(sql1);
            sta.setInt(1,star.getPostId());
            System.out.println(star.getStarAccount());
            sta.setInt(2,star.getStarAccount());
            sta.setTimestamp(3,star.getStarTime());
            result=sta.executeUpdate();
            sta=conn.prepareStatement(sql2);
            ResultSet resultSet=sta.executeQuery();
            while (resultSet.next()){
                result=resultSet.getInt(1);
            }
            sta.close();
        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("StarDaoImpl.addStar:数据库操作出问题");
        }
        return result;
    }

    @Override
    public int delStar(Star star){
        Connection conn=getConnection();
        String sql="delete from star where starId=?";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,star.getStatId());
            result=sta.executeUpdate();
        }catch (SQLException e){
            System.out.println("StarDaoImpl.delStar：数据库操作出问题了");
        }
        return  result;
    }
}
