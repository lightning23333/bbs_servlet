package dao;

import pojo.Photo;

import java.util.List;

public interface PhotoDao {
//    接收photo
    public boolean savePhoto(Photo photo);

    public List<Photo> getPhoto(int postId);

    public boolean delPhoto(int photoId);
}
