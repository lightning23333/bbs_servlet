package dao;

import org.junit.Test;
import pojo.User;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.DBUtil.closeConnection;
import static util.DBUtil.getConnection;

public class UserDaoImpl implements UserDao {

/**
 * 账号查询
  */

    @Override
    public boolean queryAccount(int account) {
        Connection conn=getConnection();
        String sql="select account from user where account=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            ResultSet result=sta.executeQuery();
            int count=0;
            while(result.next()){
                count++;
            }
            boolean sus=count==1;
            sta.close();
            result.close();
            return sus;
        }catch(SQLException e){
            System.out.println("查询账号操作出错");
        }finally {
            closeConnection(conn);
        }

        return true;
    }

    /**
     * 账号密码验证
     * @param account
     * @param psw
     * @return
     */
    @Override
    public boolean queryPsw(int account, String psw) {
        Connection conn=getConnection();
        String sql="select name from user where account=? and password=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            sta.setString(2,psw);
            ResultSet result=sta.executeQuery();
            int count=0;
            while(result.next()){
                String name=result.getString("name");
                System.out.println(name+"在尝试登录");
                count++;
            }
            boolean sus=count==1;
            sta.close();
            result.close();
            return sus;
        }catch(SQLException e){
            System.out.println("验证操作出错");
        }finally {
            closeConnection(conn);
        }
        return false;
    }

    /**
     * 通过账号查看用户信息
     * @param account
     * @return
     */
    @Override
    public User queryUser(int account) {
        Connection conn=getConnection();
        User user=null;
        String name="";
        String introduction="无简介";
        String sql="select account,name,introduction from user where account=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            ResultSet result=sta.executeQuery();
            int count=0;
            while(result.next()){
                name=result.getString("name");
                introduction=result.getString("introduction");
                count++;
            }
            //做判断是否存在用户
            if(count!=0){
                user=new User();
                user.setAccount(account);
                user.setName(name);
                user.setIntroduction(introduction);
                return user;
            }
            sta.close();
            result.close();
        }catch(SQLException e){
            System.out.println("验证操作出错");
        }finally {
            closeConnection(conn);
        }
        return null;
    }
    /**
     * 用户名查询
     */

    @Override
    public boolean queryName(String name) {
        Connection conn=getConnection();
        String sql="select name from user where name=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setString(1,name);
            ResultSet result=sta.executeQuery();
            int count=0;
            while(result.next()){
                count++;
            }
            boolean sus=count==1;
            sta.close();
            result.close();
            return sus;
        }catch(SQLException e){
            System.out.println("查询用户名操作出错");
        }finally {
            closeConnection(conn);
        }

        return false;
    }

    /**
     * 加入新用户
     * @param account
     * @param name
     * @param psw
     * @return
     */
    @Override
    public boolean insertUser(int account ,String name,String psw) {
        Connection conn=getConnection();
        String sql="insert into user(account,name,password) values(?,?,?)";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            sta.setString(2,name);
            sta.setString(3,psw);
            sta.executeUpdate();
            sta.close();
            System.out.println("插入数据操作成功");
            return true;
        }catch (SQLException e){
            System.out.println("插入数据操作失败");
        }finally {
            closeConnection(conn);
        }
        return false;
    }

    /**
     * 更新用户信息
     * @param loadUser
     * @return
     */

    @Override
    public boolean updateUser(User loadUser) {
        Connection conn=getConnection();
        String sql="update user set name=?";
        String name=loadUser.getName();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setString(1,name);
            int result=sta.executeUpdate();

            //判读是否修改成功
            if(result!=0){
                System.out.println("改了信息，变亮了");
                return true;
            }else{
                System.out.println("太难了，没改成！！！");
            }
            sta.close();
        }catch(SQLException e){
            System.out.println("信息修改错误，数据库操作出问题");
        }finally {
            closeConnection(conn);
        }
//        String name
        return false;
    }

    /**
     * 修改用户名
     * @param name
     * @return
     */

    @Override
    public boolean updateName(String name) {
        return false;
    }

    @Override
    public List<User> queryUsers() {
        List<User> users=new ArrayList<>();
        Connection conn=getConnection();
        String sql="select * from user";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            ResultSet resultSet=sta.executeQuery();
            while (resultSet.next()){
                User user=new User();
                user.setName(resultSet.getString("name"));
                user.setAccount(resultSet.getInt("account"));
                user.setHead(resultSet.getString("head"));
                user.setIntroduction(resultSet.getString("introduction")==null?"":resultSet.getString("introduction"));
                users.add(user);
            }
            sta.close();
            resultSet.close();
        }catch (SQLException e){
            System.out.println("UserDaoImpl.queryUsers:数据库操作失败");
        }
        return users;
    }

    @Override
    public boolean insertToken(int account,String token) {
        boolean result=false;
        Connection conn=getConnection();
        String sql="insert into user(token) values (?) where account=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setString(1,token);
            sta.setInt(2,account);
            result=sta.executeUpdate()==1?true:false;
        }catch (SQLException e){
            System.out.println("UserDaoImpl.queryUsers:数据库操作失败");
        }
        return result;
    }

    @Test
    public void test(){
        updateParams("introduction","月亮不睡我不睡",666666);
    }

    @Override
    public boolean updateParams(String params, String value,int account) {
        Connection conn=getConnection();
        String sql="update user set "+params+"=? where account=?";
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
//            sta.setString(1,params);
            sta.setString(1,value);
            sta.setInt(2,account);
            int result=sta.executeUpdate();
            //判读是否修改成功
            if(result!=0){
                System.out.println("改了信息，变亮了");
                return true;
            }else{
                System.out.println("太难了，没改成！！！");
            }
            sta.close();
        }catch(SQLException e){
            e.printStackTrace();
            System.out.println("信息修改错误，数据库操作出问题");
        }finally {
            closeConnection(conn);
        }
        return false;
    }
}
