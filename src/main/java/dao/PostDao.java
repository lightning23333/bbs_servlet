package dao;

import pojo.Post;

import java.util.List;

public interface PostDao {
//    依据用户账号查帖子
    public Post queryPost(int account);
//    按时间顺序返回全部帖子
    public List<Post> orderPost();
//    添加帖子
    public int insertPost(Post post);
//    修改帖子
    public boolean updatePost(Post post);
//    删除帖子
    public boolean delPost(int id);
//    小时热榜
    public List<Post> starPost();
}

