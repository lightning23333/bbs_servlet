package dao;

import pojo.User;

import java.util.List;

public interface UserDao {
    //添加新用户
    public boolean insertUser(int account,String name,String psw);
    //账号查询
    public boolean queryAccount(int account);
    //用户名查询
    public boolean queryName(String name);
    //账号密码验证
    public boolean queryPsw(int account, String psw);
    //用户信息查询
    public User queryUser(int account);
    //更新用户信息
    public boolean updateUser(User loadUser);
    //更新用户名
    public boolean updateName(String name);
//    所有用户信息查询
    public List<User> queryUsers();
    public boolean insertToken(int account,String token);
//    更新指定字段的信息
    public boolean updateParams(String params,String value,int account);
}
