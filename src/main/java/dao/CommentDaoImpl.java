package dao;


import pojo.Comment;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.DBUtil.getConnection;

public class CommentDaoImpl implements CommentDao {
    @Override
    public int insertComment(Comment comment) {
        Connection conn=getConnection();
        String sql1="insert into comment(comment_content,comment_account,comment_time,post_id) values (?,?,?,?)";
        String sql2="SELECT LAST_INSERT_ID()";
        int result=-1;
        try{
            PreparedStatement sta=conn.prepareStatement(sql1);
            sta.setString(1,comment.getCommentContent());
            sta.setInt(2,comment.getCommentAccount());
            sta.setTimestamp(3,comment.getCommentTime());
            sta.setInt(4,comment.getPostId());
            result=sta.executeUpdate();
            System.out.println(comment.getCommentAccount()+"评论数据库成功");
            sta=conn.prepareStatement(sql2);
            ResultSet resultSet=sta.executeQuery();
            while (resultSet.next()){
                result=resultSet.getInt(1);
            }
            sta.close();
        }catch (SQLException e){
            System.out.println("为"+comment.getPostId()+"添加评论数据库操作失败");
        }
        return result;
    }

    @Override
    public List<Comment> queryComments() {
        Connection conn=getConnection();
        String sql="select * from comment";
        List<Comment> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            ResultSet result=sta.executeQuery();
            while(result.next()){
                Comment comment=new Comment();
                comment.setCommentId(result.getInt("comment_id"));
                comment.setCommentContent(result.getString("comment_content"));
                comment.setCommentAccount(result.getInt("comment_account"));
                comment.setCommentTime(result.getTimestamp("comment_time"));
                comment.setPostId(result.getInt("post_id"));
                lists.add(comment);
            }
        }catch (SQLException e){
            System.out.println("查询全部评论数据库操作失败");
        }
        return lists;
    }

    @Override
    public List<Comment> useComment(int account) {
        Connection conn=getConnection();
        String sql="select * from comment where comment_account=?";
        List<Comment> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            ResultSet result=sta.executeQuery();
            while (result.next()){
                Comment comment=new Comment();
                comment.setCommentId(result.getInt("comment_id"));
                comment.setCommentAccount(account);
                comment.setCommentContent(result.getString("comment_content"));
                comment.setCommentTime(result.getTimestamp("comment_time"));
                comment.setPostId(result.getInt("post_id"));
                lists.add(comment);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询用户"+account+"全部评论数据库操作失败");
        }
        return lists;
    }

    @Override
    public List<Comment> queryComment(int postId) {
        Connection conn=getConnection();
        String sql="select * from comment where post_id=?";
        List<Comment> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,postId);
            ResultSet result=sta.executeQuery();
            while (result.next()){
                Comment comment=new Comment();
                comment.setCommentId(result.getInt("comment_id"));
                comment.setCommentAccount(result.getInt("comment_account"));
                comment.setCommentContent(result.getString("comment_content"));
                comment.setCommentTime(result.getTimestamp("comment_time"));
                comment.setPostId(postId);
                lists.add(comment);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询帖子"+postId+"全部评论数据库操作失败");
        }
        return lists;
    }

    @Override
    public boolean delComment(int commentId) {
        Connection conn=getConnection();
        String sql="delete from comment where comment_id=?";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,commentId);
            result=sta.executeUpdate();
            sta.close();
        }catch (SQLException e){
            e.printStackTrace();
            System.out.println("删除评论"+commentId+"数据库操作失败");
        }
        return result==1?true:false;
    }

}
