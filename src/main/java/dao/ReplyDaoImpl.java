package dao;


import pojo.Reply;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static util.DBUtil.getConnection;

public class ReplyDaoImpl implements ReplyDao {
    @Override
    public List<Reply> queryReply(int commentId) {
        Connection conn=getConnection();
        String sql="select * from reply where comment_id=?";
        List<Reply> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,commentId);
            ResultSet result=sta.executeQuery();
            while (result.next()){
                Reply reply=new Reply();
                reply.setReplyId(result.getInt("reply_id"));
                reply.setReplyAccount(result.getInt("reply_account"));
                reply.setReplyContent(result.getString("reply_content"));
                reply.setReplyTime(result.getTimestamp("reply_time"));
                reply.setCommentId(result.getInt("comment_id"));
                lists.add(reply);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询评论"+commentId+"全部回复数据库操作失败");
        }
        return lists;
    }

    @Override
    public boolean insertReply(Reply reply) {
        Connection conn=getConnection();
        String sql="insert into reply(reply_account,reply_content,reply_time,comment_id) values (?,?,?,?)";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,reply.getReplyAccount());
            sta.setString(2,reply.getReplyContent());
            sta.setTimestamp(3,reply.getReplyTime());
            sta.setInt(4,reply.getCommentId());
            result=sta.executeUpdate();
            sta.close();
        }catch (SQLException e){
            System.out.println("为"+reply.getReplyId()+"添加评论数据库操作失败");
        }
        return result==1?true:false;
    }

    @Override
    public boolean delReply(int replyId) {
        Connection conn=getConnection();
        String sql="delete from reply where reply_id=?";
        int result=0;
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,replyId);
            result=sta.executeUpdate();
            sta.close();
        }catch (SQLException e){
            System.out.println("删除回复"+replyId+"操作失败");
        }
        return result==1?true:false;
    }

    @Override
    public List<Reply> useReply(int account) {
        Connection conn=getConnection();
        String sql="select * from reply where reply_account=?";
        List<Reply> lists=new ArrayList<>();
        try{
            PreparedStatement sta=conn.prepareStatement(sql);
            sta.setInt(1,account);
            ResultSet result=sta.executeQuery();
            while (result.next()){
                Reply reply=new Reply();
                reply.setReplyId(result.getInt("reply_id"));
                reply.setReplyAccount(result.getInt("reply_account"));
                reply.setReplyContent(result.getString("reply_content"));
                reply.setReplyTime(result.getTimestamp("reply_time"));
                reply.setCommentId(result.getInt("comment_id"));
                lists.add(reply);
            }
            result.close();
            sta.close();
        }catch (SQLException e){
            System.out.println("查询用户"+account+"数据库操作失败");
        }
        return lists;
    }
}
