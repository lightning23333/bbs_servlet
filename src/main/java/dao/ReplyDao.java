package dao;

import pojo.Comment;
import pojo.Reply;

import java.util.List;

public interface ReplyDao {
    //读取某评论的回复
    public List<Reply> queryReply(int commentId);
    //添加回复
    public boolean insertReply(Reply reply);
    //删除评论
    public boolean delReply(int replyId);
    //读取某用户的全部回复
    public List<Reply> useReply(int account);
}
