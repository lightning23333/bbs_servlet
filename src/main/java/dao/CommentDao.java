package dao;

import pojo.Comment;

import java.util.List;

public interface CommentDao {
    //读取某帖子全部评论
    public List<Comment> queryComment(int postId);
    //添加评论
    public int insertComment(Comment comment);
    //删除评论
    public boolean delComment(int commentId);
    //读取某用户的全部评论
    public List<Comment> useComment(int account);
    //查询全部帖子
    public List<Comment> queryComments();
}
