package dao;

import pojo.Star;

import java.util.List;

public interface StarDao {
//    查询指定帖子点赞情况
    public List<Star> postStar(int postId);
//    查询指定用户点赞情况
    public List<Star> useStar(int account);
//    查询点赞数据库中所有信息
    public List<Star> queryAll();
    public int addStar(Star star);
    public int delStar(Star star);
}
