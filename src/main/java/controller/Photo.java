package controller;

import org.junit.Test;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class Photo {

    private String imagesPath = new File("").getCanonicalPath()+"\\web\\images";

    public Photo() throws IOException {
    }

    public void savePhoto(String postId, String value, int postion, InputStream is) throws IOException {
        String path=imagesPath+"\\postId\\";
        File fileDir=new File(path);

        if (!fileDir.exists()) { //如果不存在 则创建
            fileDir.mkdirs();
        }
        String url=path+postId+"\\"+postId+"_"+postion+".jpg";
        File file = new File(url);
        if(!file.exists()){
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println(url + "文件不存在");
        }
        FileOutputStream os = new FileOutputStream(file, true);
        byte[] bytesArray = value.getBytes();
        // 开始读取
        int len;
        while ((len = is.read(bytesArray)) != -1) {
            os.write(bytesArray, 0, len);
        }
        os.close();
        is.close();
    }
    @Test
    public void test(){
        System.out.println(imagesPath);
    }



    public String getPhoto(){
        return null;
    }

}
