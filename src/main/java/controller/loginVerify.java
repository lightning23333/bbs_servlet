package controller;

import dao.UserDao;
import dao.UserDaoImpl;

public class loginVerify {
    private String psw;
    private int account;
    private UserDao dao;
    public loginVerify(int account, String psw){
        this.account=account;
        this.psw=psw;
    }
    public int start(){
        int status=0;
        if(isEmpty()){
            if(isSure()){
                status=1;
            }else{
                status=2;
            }
        }
        return status;
    }
    public boolean isEmpty(){
        dao=new UserDaoImpl();
        if(dao.queryAccount(account)){
            System.out.println("用户存在");
            return true;
        }else{
            System.out.println("用户不存在");
            return false;
        }
    }

    public boolean isSure(){
        dao=new UserDaoImpl();
        if(dao.queryPsw(account,psw)){
            System.out.println("用户登录成功");
            return true;
        }else{
            System.out.println("登录失败");
            return false;
        }
    }
}
