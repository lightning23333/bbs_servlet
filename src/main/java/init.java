import redis.RefreshRedis;

import javax.servlet.annotation.WebListener;

@WebListener
public class init{
    static {
        RefreshRedis refreshRedis=new RefreshRedis();
        refreshRedis.refreshPost();
        refreshRedis.refreshStar();
        refreshRedis.refreshComment();
        refreshRedis.refreshUser();
        refreshRedis.refreshToken();
   }
}
