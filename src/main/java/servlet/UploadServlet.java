package servlet;



import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;
import org.json.JSONObject;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.*;
import java.util.List;

public class UploadServlet extends HttpServlet {
    private String postId;
    private String type;
    // 修改用户的图片
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        int message = 404;
        try{
            DiskFileItemFactory dff = new DiskFileItemFactory();
            ServletFileUpload sfu = new ServletFileUpload(dff);
            List<FileItem> items = sfu.parseRequest(new ServletRequestContext(request));
            for(FileItem item:items){
                if(item.isFormField()){
                    switch (item.getFieldName()){
                        case "type":
                            type = item.getString();
                            break;
                        case "postId":
                            //普通表单
                            postId=item.getString();
                            break;
                        default:
                    }

                } else {// 获取上传字段
                    // 更改文件名为唯一的
                    String filename = item.getName();
                    String files="error";
                    // 生成存储路径
                    System.out.println(type);
                    if(type.equals("303")){
                        files="head";
                    }else {
                        files=postId;
                    }
                    String storeDirectory = request.getSession().getServletContext().getRealPath("/images/"+files);
                    System.out.println(storeDirectory);
                    File file = new File(storeDirectory);
                    if (!file.exists()) {
                        file.mkdir();
                    }
                    String path = storeDirectory+"/"+filename;
                    // 处理文件的上传
                    try {
                        item.write(new File(path));
                        message=200;
                    } catch (Exception e) {
                        e.printStackTrace();
                        message = 301;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            message = 404;
        } finally {
            System.out.println(message);
            JSONObject obj=new JSONObject();
            obj.put("result",message);
            response.setContentType("text/json; charset=utf-8");
            response.getWriter().print(obj);
        }
    }
}
