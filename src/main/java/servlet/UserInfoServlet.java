package servlet;

import bean.UserInfo;
import org.json.JSONObject;
import redis.RedisDao;
import redis.RedisDaoImpl;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class UserInfoServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        int account=Integer.parseInt(req.getParameter("account"));
        RedisDao redisDao=new RedisDaoImpl();
        UserInfo userInfo=redisDao.getUserInfo(account);
        JSONObject obj=new JSONObject();
        obj.put("account",userInfo.getAccount());
        obj.put("name",userInfo.getName());
        System.out.println("hello");
        System.out.println(userInfo.getIntroduction());
        obj.put("introduction",userInfo.getIntroduction());
        obj.put("head",userInfo.getHead());
        resp.setContentType("text/json; charset=utf-8");
//        输出数据
        PrintWriter out = resp.getWriter();
        out.print(obj);
    }
}
