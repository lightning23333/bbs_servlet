package redis;

import dao.*;
import pojo.Comment;
import pojo.Post;
import pojo.Star;
import pojo.User;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import java.util.List;

import static util.REUtile.getRedis;

public class RefreshRedis {
    private final Jedis jedis;

    public RefreshRedis(){
        jedis=getRedis();
    }

//    刷新缓存中的帖子
    public boolean refreshPost(){
//        选中数据库0
        jedis.select(0);

//        开始刷新缓存里的数据
        PostDao dao=new PostDaoImpl();

//        返回排好序的帖子
        List<Post> posts=dao.orderPost();
//        先删除所有数据
        jedis.flushDB();
        for(Post post:posts){
            String id=String.valueOf(post.getPostId());
            String account=String.valueOf(post.getPostAccount());
            String title=post.getPostTitle();
            String type=String.valueOf(post.getPostType());
            String content=post.getPostContent();
            String time=String.valueOf(post.getProTimer());
            String star=String.valueOf(post.getStarNum());


            try{
                jedis.rpush("order",id);
                jedis.rpush(id,id);
                jedis.rpush(id,account);
                jedis.rpush(id,title);
                jedis.rpush(id,type);
                jedis.rpush(id,content);
                jedis.rpush(id,time);
                jedis.rpush(id,star);

                jedis.rpush(account+"_account",id);
//                用序列化的方式将post存到redis中
//                jedis.set(key.getBytes(), SerializeUtils.serialize(post));
//                byte[] byt = jedis.get(key.getBytes());
//                Post obj = (Post)SerializeUtils.unserizlize(byt);
//                System.out.println(obj.getPostId());

//                System.out.println("打印一下结果"+jedis.lrange(id, 0, -1));

            }catch (JedisException e){
                e.printStackTrace();
                System.out.println("添加redis操作失败");
                return false;
            }


        }
        System.out.println("添加操作完成");
        return true;
    }

//    刷新缓存里的评论信息
    public void refreshComment(){
        jedis.select(1);
//        先删除所有数据
        jedis.flushDB();

//        开始刷新缓存里评论信息
        CommentDao comDao=new CommentDaoImpl();
        List<Comment> lists=comDao.queryComments();
        for(Comment comment:lists){
            try{
                //帖子号与评论号对应关系
                jedis.rpush(comment.getPostId()+"_key",String.valueOf(comment.getCommentId()));
                //帖子对应的评论内容
                jedis.rpush(String.valueOf(comment.getCommentId()),comment.getCommentContent(),String.valueOf(comment.getCommentAccount()),String.valueOf(comment.getCommentTime()));
//                //帖子对应的评论者
//                jedis.rpush(String.valueOf(comment.getCommentId()),String.valueOf(comment.getCommentAccount()));
//                //评论对应的时间
//                jedis.rpush(String.valueOf(comment.getCommentId()),String.valueOf(comment.getCommentTime()));

            }catch (JedisException e){
                System.out.println("刷新评论信息redis操作失败");
            }
        }
        System.out.println("刷新评论信息redis操作成功");
    }

//    刷新点赞信息
    public void refreshStar(){
//        选中数据库2
        jedis.select(2);

//        先删除当前数据库数据
        jedis.flushDB();

//        从mysql中拿到数据
        StarDao dao=new StarDaoImpl();
        List<Star> lists=dao.queryAll();

//        进行遍历加载到redis中
        for(Star star:lists){
            try{
//                查询指定帖子的所有点赞者账号
//                jedis.rpush(star.getPostId()+"_starAc",String.valueOf(star.getStarAccount()));
//                jedis.rpush(star.getPostId()+"_starTime",String.valueOf(star.getStarTime()));
                String starId=String.valueOf(star.getStatId());
                String starAccount=String.valueOf(star.getStarAccount());
                String postId=String.valueOf(star.getPostId());
                jedis.rpush(postId+"_key",starId);
                jedis.rpush(starId,starAccount);
                jedis.rpush(starId,String.valueOf(star.getStarTime()));

                jedis.rpush(starAccount+"_account",postId);

            }catch (JedisException e){
                System.out.println("刷新redis中点赞信息操作失败");
            }
        }
        System.out.println("刷新redis点赞信息操作完成");
    }
//    刷新用户信息
    public void refreshUser(){
        jedis.select(4);

        //        先删除当前数据库数据
        jedis.flushDB();
        UserDao userDao=new UserDaoImpl();
        List<User> users=userDao.queryUsers();
        String account;
        for(User user:users){
            account=String.valueOf(user.getAccount());
            jedis.rpush(account,user.getName());
//            对可有可无数据进行处理
            if(user.getHead()!=null){
                jedis.rpush(account,user.getHead());
            }else{
                jedis.rpush(account,"0");
            }
            if(user.getIntroduction()!=null){
                jedis.rpush(account,user.getIntroduction());
            }else{
                jedis.rpush(account,"这个人很懒什么也没有留下");
            }

    }
        System.out.println("刷新redis用户信息操作完成");
    }

    public void refreshToken(){
        jedis.select(5);
        jedis.flushDB();
    }
////    更新点赞数目
//    public void updateStar(int postId,int account,int dao){
//        System.out.println("==========");
//        System.out.println(account+"用户想给帖子"+postId+"的赞"+dao);
//        List<String> m=jedis.lrange(String.valueOf(postId),6,7);
//        int starNum=Integer.valueOf(m.get(0));
//        int result=starNum+dao;
//        jedis.lset(String.valueOf(postId),6,String.valueOf(result));
//        System.out.println("操作成功");
//        System.out.println(postId+":赞的数量变成了"+result);
//    }
}
