package redis;

import bean.UserInfo;
import pojo.Comment;
import pojo.Post;
import pojo.Star;
import pojo.User;

import java.sql.Timestamp;
import java.util.List;

public interface RedisDao {
//    用户信息
    public boolean insertUser(int account,String name);
    public UserInfo getUserInfo(int account);
    public String getToken(int account);
    public boolean isToken(int account);
    public boolean addToken(int account,String token);
//    修改用户信息
    public boolean updateUserInfo(int index,int account,String value);
//    添加在线用户
    public boolean addOnline(int account);
//    用户离线
    public boolean delOnline(int account);


//    添加帖子
    public boolean addPost(Post post);
//    删除帖子
    public boolean delPost(int postId,int account);
//    读取一条帖子数据
    public Post getPost(int postId);
//    读取缓存中指定用户指定范围帖子的id
    public List<String> getUserPost(int account,int start,int end);
//    读取缓存中指定范围的帖子id
    public List<String> getPostId(int start,int end);
//    获取帖子的总数
    public int postCount();
    public int userPostCount(int account);
    public int starPostCount(int account);
    public boolean addUserPost(int postId,int account);


//    帖子点赞
    public boolean addStar(Star star);
//    帖子取消点赞
    public boolean unStar(int postId,int account);
//    获取指定帖子的点赞信息
    public List<Star> getStar(int postId);
//    判断指定帖子下指定用户是否存在
    public boolean queryStar(int postId,int account);
//    获取指定用户点赞过帖子id
    public List<String> getUserStar(int account,int start,int end);


//    添加评论
    public boolean addCom(Comment comment);
//    删除评论
    public boolean delCom(int commentId);
//    读取评论
    public List<Comment> getCom(int postId);



}
