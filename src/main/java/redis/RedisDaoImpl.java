package redis;

import bean.UserInfo;
import dao.CommentDao;
import dao.CommentDaoImpl;
import org.junit.Test;
import pojo.Comment;
import pojo.Post;
import pojo.Star;
import pojo.User;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static util.REUtile.getRedis;

public class RedisDaoImpl implements RedisDao{
    private final Jedis jedis=getRedis();

    @Override
    public boolean insertUser(int account,String name) {
        try{
            jedis.select(4);
            jedis.rpush(String.valueOf(account),name);
            //            对可有可无数据进行处理
            jedis.rpush(String.valueOf(account),"0");
            jedis.rpush(String.valueOf(account),"这个人很懒什么也没有留下");
            return true;
        }catch (Exception e){
            return false;
        }

    }

    @Override
    public UserInfo getUserInfo(int account) {
        UserInfo userInfo=new UserInfo();
        jedis.select(4);
        List<String> list=jedis.lrange(String.valueOf(account),0,-1);
        if(list.size()!=0){
            userInfo.setAccount(account);
            userInfo.setName(list.get(0));
            userInfo.setHead(list.get(1));
            if(list.size()<2){
                return userInfo;
            }else {
                userInfo.setIntroduction(list.get(2));
            }

        }
        return userInfo;
    }

    @Override
    public String getToken(int account) {
        String token;
        jedis.select(5);
        token=jedis.get(String.valueOf(account));
        return token;
    }

    @Override
    public boolean isToken(int account) {
        jedis.select(5);
        return jedis.exists(String.valueOf(account));
    }

    @Override
    public boolean addToken(int account,String token) {
        try{
            jedis.select(5);
            jedis.set(String.valueOf(account),token);
        }catch (JedisException e){
            System.out.println("RedisDaoImpl.addToken方法出错");
            return false;
        }
        return true;
    }

    @Test
    public void test(){
        System.out.println(updateUserInfo(1,666666,"1"));
    }
    @Override
    public boolean updateUserInfo(int index,int account, String value) {
        jedis.select(4);
        return jedis.lset(String.valueOf(account),index,value).equals("OK");
    }

    @Override
    public boolean addOnline(int account) {
        jedis.select(3);
        try{
            jedis.sadd("online",String.valueOf(account));
        }catch (JedisException e){
            System.out.println("redis4:添加用户在线信息成功");
            return false;
        }
        return true;
    }

    @Override
    public boolean delOnline(int account) {
        jedis.select(3);
        try{
            jedis.srem("online",String.valueOf(account));
        }catch (JedisException e){
            System.out.println("redis4:删除用户在线信息成功");
            return false;
        }
        return true;
    }

    @Override
    public boolean queryStar(int postId, int account) {
        List<Star> stars=getStar(postId);
        return stars.contains(String.valueOf(account));
    }

    @Override
    public List<String> getUserStar(int account, int start, int end) {
        jedis.select(2);
        List<String> list=jedis.lrange(account+"_account",start,end);
        return list;
    }

    @Override
    public List<Comment> getCom(int postId) {
        List<Comment> comments=new ArrayList<>();
        jedis.select(1);
        List<String> commentId=jedis.lrange(String.valueOf(postId)+"_key",0,-1);
        for(String coms:commentId){
            List<String> comment=jedis.lrange(coms,0,-1);
            Comment com=new Comment();
            com.setCommentId(Integer.parseInt(coms));
            com.setCommentContent(comment.get(0));
            com.setCommentAccount(Integer.parseInt(comment.get(1)));
            com.setCommentTime(Timestamp.valueOf(comment.get(2)));
            com.setCmtName(getUserInfo(Integer.parseInt(comment.get(1))).getName());
            jedis.select(1);
            comments.add(com);
        }
        return comments;
    }

    @Override
    public boolean addPost(Post post) {
        jedis.select(0);
        String id=String.valueOf(post.getPostId());
        String account=String.valueOf(post.getPostAccount());
        String title=post.getPostTitle();
        String type=String.valueOf(post.getPostType());
        String content=post.getPostContent();
        String time=String.valueOf(post.getProTimer());
        String star=String.valueOf(post.getStarNum());
        try{
            jedis.rpush(id,id);
            jedis.rpush(id,account);
            jedis.rpush(id,title);
            jedis.rpush(id,type);
            jedis.rpush(id,content);
            jedis.rpush(id,time);
            jedis.rpush(id,star);
//                用序列化的方式将post存到redis中
//                jedis.set(key.getBytes(), SerializeUtils.serialize(post));
//                byte[] byt = jedis.get(key.getBytes());
//                Post obj = (Post)SerializeUtils.unserizlize(byt);
//                System.out.println(obj.getPostId());
//            System.out.println("添加操作完成");
//            System.out.println("打印一下结果"+jedis.lrange(id, 0, -1));

        }catch (JedisException e){
            e.printStackTrace();
            System.out.println("添加redis操作失败");
            return false;
        }
        return true;
    }

    @Override
    public boolean delPost(int postId,int account) {
        jedis.select(0);
        if(jedis.del(String.valueOf(postId))==1L){
            jedis.lrem(account+"_account",1, String.valueOf(postId));
            jedis.lrem("order",1,String.valueOf(postId));
            jedis.select(1);
            List<String> cmts=jedis.lrange(postId+"_key",0,-1);
            jedis.del(postId+"_key");
            for(String cmt:cmts){
                jedis.del(cmt);
            }
            jedis.select(2);
            List<String> stars=jedis.lrange(postId+"_key",0,-1);
            jedis.del(postId+"_key");
            for(String star:stars){
                jedis.del(star);
            }
            return true;
        }
        return false;
    }

    @Override
    public Post getPost(int postId) {
        Post post=new Post();
        jedis.select(0);
        List<String> lists=jedis.lrange(String.valueOf(postId),0,-1);
        post.setPostId(postId);
        System.out.println(lists.toString());
        post.setPostAccount(Integer.parseInt(lists.get(1)));
        post.setPostTitle(lists.get(2));
        post.setPostType(Integer.parseInt(lists.get(3)));
        post.setPostContent(lists.get(4));
        post.setProTimer(Timestamp.valueOf(lists.get(5)));
        return post;
    }


    @Override
    public List<String> getUserPost(int account, int start, int end) {
        jedis.select(0);
        List<String> list=jedis.lrange(account+"_account",start,end);
        return list;
    }
    @Override
    public boolean addUserPost(int postId, int account){
        try{
            jedis.select(0);
            jedis.rpush(account+"_account", String.valueOf(postId));
            return true;
        }catch (Exception e){
            return false;
        }

    }

    public List<Post> getPostAll(int start, int end) {
        List<Post> posts=new ArrayList<>();
        jedis.select(0);
        List<String> postId=jedis.lrange("order",start,end);
        for(String a:postId){
            List<String> lists=jedis.lrange(a,0,-1);
            Post post=new Post();
            post.setPostId(Integer.valueOf(a));
            post.setPostAccount(Integer.parseInt(lists.get(1)));
            post.setPostTitle(lists.get(2));
            post.setPostType(Integer.parseInt(lists.get(3)));
            post.setPostContent(lists.get(4));
            post.setProTimer(Timestamp.valueOf(lists.get(5)));
            posts.add(post);
        }
        return posts;
    }

    @Override
    public List<String> getPostId(int start, int end) {

        jedis.select(0);
        List<String> list=jedis.lrange("order",start,end);
        return list;
    }

    @Override
    public int postCount() {
        jedis.select(0);
        Long count=jedis.llen("order");
        return count.intValue();
    }

    @Override
    public int userPostCount(int account) {
        jedis.select(0);
        Long count=jedis.llen(account+"_account");
        return count.intValue();
    }

    @Override
    public int starPostCount(int account) {
        jedis.select(2);
        Long count=jedis.llen(account+"_account");
        return count.intValue();
    }

    @Override
    public boolean addStar(Star star) {
        jedis.select(2);
        try{
            jedis.rpush(star.getPostId()+"_key",String.valueOf(star.getStatId()));
            jedis.rpush(String.valueOf(star.getStatId()),String.valueOf(star.getStarAccount()));
            jedis.rpush(String.valueOf(star.getStatId()),String.valueOf(star.getStarTime()));
            jedis.rpush(star.getStarAccount()+"_account",String.valueOf(star.getPostId()));
            return true;
        }catch (JedisException e){
            return false;
        }
    }

    @Override
    public boolean addCom(Comment comment) {
        jedis.select(1);
        try{
            CommentDao commentDao=new CommentDaoImpl();
            int cmtId=comment.getCommentId();
            jedis.rpush(String.valueOf(comment.getPostId())+"_key",String.valueOf(cmtId));
            //帖子对应的评论内容
            jedis.rpush(String.valueOf(cmtId),comment.getCommentContent(),String.valueOf(comment.getCommentAccount()),String.valueOf(comment.getCommentTime()));
            return true;
        }catch (JedisException e){
            System.out.println("redis1:添加评论操作失败");
            return false;
        }

    }

    @Override
    public List<Star> getStar(int postId) {
        jedis.select(2);
        List<Star> list=new ArrayList<>();
        List<String> stringList=jedis.lrange(postId+"_key",0,-1);
        for(String starId:stringList){
            List<String> strings=jedis.lrange(starId,0,-1);
            Star star=new Star();
            star.setPostId(postId);
            star.setStarId(Integer.valueOf(starId));
            star.setStarAccount(Integer.valueOf(strings.get(0)));
            star.setStarTime(Timestamp.valueOf(strings.get(1)));
            star.setName(getUserInfo(Integer.parseInt(strings.get(0))).getName());
            jedis.select(2);
            list.add(star);
        }
        return list;
    }

    @Override
    public boolean delCom(int comment) {
        return false;
    }

    @Override
    public boolean unStar(int postId, int account) {
        jedis.select(2);
        String act=String.valueOf(account);
        try{
            List<String> lists=jedis.lrange(postId+"_key",0,-1);
            for(int i=0;i<lists.size();i++){
                String starId=lists.get(i);
                String act1=jedis.lindex(starId,0);

                if(act1.equals(act)){
                    jedis.del(starId);
                    System.out.println(starId);
                    jedis.lrem(postId+"_key",1,starId);
                    break;
                }
            }
            System.out.println("redis6:删除点赞信息操作成功");
            return true;
        }catch (JedisException e){
            e.printStackTrace();
            System.out.println("redis6:删除点赞信息操作失败");
            return false;
        }

    }
}
