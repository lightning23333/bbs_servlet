package pojo;

import java.sql.Timestamp;
import java.util.Date;

public class Comment {
    private int commentId;
    private String commentContent;
    private int commentAccount;
    private Timestamp commentTime;
    private int postId;
    private String cmtName;

    public String getCmtName() {
        return cmtName;
    }

    public void setCmtName(String cmtName) {
        this.cmtName = cmtName;
    }


    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getCommentContent() {
        return commentContent;
    }

    public void setCommentContent(String commentContent) {
        this.commentContent = commentContent;
    }

    public int getCommentAccount() {
        return commentAccount;
    }

    public void setCommentAccount(int commentAccount) {
        this.commentAccount = commentAccount;
    }

    public Timestamp getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(Timestamp commentTime) {
        this.commentTime = commentTime;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }
}
