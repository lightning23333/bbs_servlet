package pojo;

import java.sql.Timestamp;
import java.util.Date;

public class Star {
    private int statId;
    private int postId;
    private int starAccount;
    private Timestamp starTime;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;

    public int getStatId() {
        return statId;
    }

    public void setStarId(int statId) {
        this.statId = statId;
    }

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getStarAccount() {
        return starAccount;
    }

    public void setStarAccount(int starAccount) {
        this.starAccount = starAccount;
    }

    public Timestamp getStarTime() {
        return starTime;
    }

    public void setStarTime(Timestamp starTime) {
        this.starTime = starTime;
    }
}
