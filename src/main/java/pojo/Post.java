package pojo;


import java.io.Serializable;
import java.sql.Timestamp;

public class Post implements Serializable {
    private int postId;
    private int postAccount;
    private String postTitle;
    private int postType;
    private String postContent;
    private Timestamp proTimer;
    private int starNum;

    public int getPostId() {
        return postId;
    }

    public void setPostId(int postId) {
        this.postId = postId;
    }

    public int getPostAccount() {
        return postAccount;
    }

    public void setPostAccount(int postAccount) {
        this.postAccount = postAccount;
    }

    public String getPostTitle() {
        return postTitle;
    }

    public void setPostTitle(String postTitle) {
        this.postTitle = postTitle;
    }

    public int getPostType() {
        return postType;
    }

    public void setPostType(int postType) {
        this.postType = postType;
    }

    public String getPostContent() {
        return postContent;
    }

    public void setPostContent(String postContent) {
        this.postContent = postContent;
    }

    public Timestamp getProTimer() {
        return proTimer;
    }

    public void setProTimer(Timestamp proTimer) {
        this.proTimer = proTimer;
    }

    public int getStarNum() {
        return starNum;
    }

    public void setStarNum(int starNum) {
        this.starNum = starNum;
    }

}
