import redis.RefreshRedis;
import dao.*;
import pojo.Comment;
import pojo.Post;
import pojo.Reply;

import java.sql.*;
import java.util.List;

public class Main {
    public static void main(String args[]) throws Exception {
//        DBUtil util=new DBUtil();
//        Connection conn=util.getConnection();
//        String sql="select * from bbs";
//        try {
//            Statement stat=conn.createStatement();
//            ResultSet result=stat.executeQuery(sql);
//            while(result.next()){
//                System.out.println(result.getInt("id"));
//                System.out.println(result.getString("bbs_title"));
//            }
//            System.out.println("数据库连接成功");
//        }catch (Exception e){
//            e.printStackTrace();
//        }
//        String token=new Jwt().createjwt(7963);
//        System.out.println(token);

        //连接本地的 Redis 服务
//        Jedis jedis = new Jedis("localhost");
//        System.out.println("连接成功");
//        //查看服务是否运行
//        System.out.println("服务正在运行: "+jedis.ping());
//        //存储数据到列表中
//        jedis.lpush("site-list", "Runoob");
//        jedis.lpush("site-list", "Google");
////        jedis.lpush("site-list", "Taobao");
//        // 获取存储的数据并输出
//        List<String> list = jedis.lrange("site-list", 0 ,0);
//        for(int i=0; i<list.size(); i++) {
//            System.out.println("列表项为: "+list.get(i));
//        }
//        // 获取数据并输出
//        Set<String> keys = jedis.keys("*");
//        Iterator<String> it=keys.iterator() ;
//        while(it.hasNext()){
//            String key = it.next();
//            System.out.println(key);
//        }
        RefreshRedis refreshRedisDao =new RefreshRedis();
        refreshRedisDao.refreshPost();
//        redisDao.updateStar(1,3333,1);

//        插入帖子
//        insert();

//        更新帖子
//        update();

//        删除帖子
//        delete();

//        comdao();

//        replyDao();
    }

    public static void insert(){
        PostDao dao=new PostDaoImpl();
        Post post=new Post();
        post.setPostId(3);
        post.setPostAccount(223);
        post.setPostTitle("第三条");
        post.setPostType(0);
        post.setPostContent("内容");
        post.setProTimer(new Timestamp(System.currentTimeMillis()));
        dao.insertPost(post);
    }
    public static void update(){
        PostDao dao=new PostDaoImpl();
        Post post=new Post();
        post.setPostId(3);
        post.setPostAccount(223);
        post.setPostTitle("第三条");
        post.setPostType(0);
        post.setPostContent("内容已修改");
        post.setProTimer(new Timestamp(System.currentTimeMillis()));
        dao.updatePost(post);
    }
//    删除帖子
    public static void delete(){
        PostDao dao=new PostDaoImpl();
        if(dao.delPost(2)){
            System.out.println("ok");
        }
    }
    public static void comdao(){
        CommentDao dao=new CommentDaoImpl();

//        评论添加操作
//        Comment comment=new Comment();
//        comment.setPostId(6);
//        comment.setCommentAccount(123);
//        comment.setCommentContent("添加的评论");
//        comment.setCommentTime(new Timestamp(new Date().getTime()));
//        dao.insertComment(comment);

//        删除评论
//        dao.delComment(2);

//        读取帖子的评论
//        List<Comment> lists=dao.queryComment(1);
//        for(Comment comment:lists){
//            System.out.println(comment.getPostId());
//            System.out.println(comment.getCommentAccount());
//            System.out.println(comment.getCommentContent());
//            System.out.println(comment.getCommentTime());
//        }

//        读取用户的评论
        List<Comment> lists=dao.useComment(123);
        for(Comment comment:lists) {
            System.out.println(comment.getPostId());
            System.out.println(comment.getCommentAccount());
            System.out.println(comment.getCommentContent());
            System.out.println(comment.getCommentTime());
        }
    }

    public static void replyDao(){
        ReplyDao dao=new ReplyDaoImpl();

//        添加回复
//        Reply reply=new Reply();
//        reply.setCommentId(223);
//        reply.setReplyTime(new Timestamp(new Date().getTime()));
//        reply.setReplyContent("插入的回复");
//        reply.setReplyAccount(556);
//        dao.insertReply(reply);

//        删除回复
//        dao.delReply(2);

//        更具帖子查询回复
//        List<Reply> lists=dao.queryReply(1);
//        for(Reply reply:lists){
//            System.out.println(reply.getReplyId());
//            System.out.println(reply.getReplyAccount());
//            System.out.println(reply.getReplyContent());
//            System.out.println(reply.getReplyTime());
//            System.out.println(reply.getCommentId());
//        }

//        根据用户查询回复
        List<Reply> lists=dao.useReply(7932);
        for(Reply reply:lists){
            System.out.println(reply.getReplyId());
            System.out.println(reply.getReplyAccount());
            System.out.println(reply.getReplyContent());
            System.out.println(reply.getReplyTime());
            System.out.println(reply.getCommentId());
        }

    }

}
