package util;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.exceptions.JedisException;

public class REUtile {
    public static Jedis getRedis(){
        Jedis jedis = new Jedis("localhost");
        try{
            jedis.ping();
        }catch(JedisException e){
            System.out.println("redis连接失败");
        }
        return jedis;
    }
}
