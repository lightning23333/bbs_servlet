package util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DBUtil {
    private static String driverClass;
    private static String url;
    private static String user;
    private static String pwd;

//    //static block静态块
//    static{
//        //产生properties类的实例
//        Properties prop = new Properties();
//        //转换成文件流
//        InputStream is = sqlUtil.class.getClassLoader().getResourceAsStream("util/config.properties");
//        System.out.println(sqlUtil.class.getClassLoader().getResourceAsStream("util/config.properties"));
//        try {
//            prop.load(is);
//            driverClass = prop.getProperty("driverClass").trim();
//            url = prop.getProperty("url").trim();
//            user = prop.getProperty("user").trim();
//            pwd = prop.getProperty("pwd").trim();
//            Class.forName(driverClass);
//
//            //当前类的位置
//            System.out.println(sqlUtil.class.getResource(""));
//            //当前类的根目录的位置
//            System.out.println(sqlUtil.class.getClassLoader().getResource(""));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
    static{
        driverClass="com.mysql.cj.jdbc.Driver";
        url="jdbc:mysql://localhost:3306/android_demo?useUnicode=true&characterEncoding=utf-8&serverTimezone=GMT%2B8&useSSL=false";
        user="root";
        pwd="123456";
        try{
            Class.forName(driverClass);
        }catch(Exception e){
            System.out.println("驱动加载出错");
        }
}

    public static Connection getConnection(){

        // 数据库的连接
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user,pwd);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }
    public static void closeConnection(Connection conn){
        if (conn != null) {
            try {
                conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
//        conn.close();
    }
}
