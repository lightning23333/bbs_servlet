package timeTask;

import java.util.Timer;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServlet;



/**
 * Servlet implementation class TimerTaskListenerServlet
 */

public class TimerTaskListener implements ServletContextListener {
    private static final long serialVersionUID = 1L;

    private Timer timer;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TimerTaskListener() {
        super();
    }


    @Override
    public void contextDestroyed(ServletContextEvent event) {
        if(timer  != null) {
            timer.cancel();
            event.getServletContext().log("timer destroy");
        }
    }

    @Override
    public void contextInitialized(ServletContextEvent event) {
        timer = new Timer(true);
        event.getServletContext().log("timer start");
        //一小时刷新一次
        timer.schedule(new Timing(), 0,3600000);
        event.getServletContext().log("task has been added");
    }

}
