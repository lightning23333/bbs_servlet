package timeTask;

import org.junit.Test;
import redis.RefreshRedis;

import javax.servlet.ServletContext;
import java.util.*;

public class Timing extends TimerTask {

    private ServletContext servletContext;
    private static boolean isRunning = false;

    public Timing(){

    }
    public Timing(ServletContext servletContext) {
        this.servletContext = servletContext;
    }
    @Override
    public void run() {
        System.out.println("定时刷新redis功能开始");
        RefreshRedis dao=new RefreshRedis();
        dao.refreshPost();
        dao.refreshComment();
        dao.refreshStar();
    }

}
